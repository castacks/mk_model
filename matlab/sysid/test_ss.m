function test_ss(mdl,input,ref_out)

X = zeros(size(mdl.B));

output = zeros(size(input));

for t = 1:length(input)

    output(t) = mdl.C*X;
    X = mdl.A*X + mdl.B*input(t);
    
end

plot(ref_out,'r');
hold on;
plot(output);

end