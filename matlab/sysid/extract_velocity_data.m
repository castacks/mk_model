function [thrust_cmd, gps_nvel, gps_evel, gps_dvel, t] = extract_velocity_data(dir,sample_period)

ppm = csvread([dir '/ppm.csv'],1,0);
gps = csvread([dir '/gps.csv'],1,0);

ppm_time = ppm(:,1);
thrust_cmd = ppm(:,4);

gps_time = gps(:,1);
gps_nvel = gps(:,11);
gps_evel = gps(:,12);
gps_dvel = gps(:,13);


thrust_cmd = thrust_cmd/32768;


% Align time base
gps_time = gps_time - gps_time(1);
ppm_time = ppm_time - ppm_time(1);

% Convert nanoseconds to seconds
gps_time = gps_time/1e9;
ppm_time = ppm_time/1e9;

% Create a time base
start_time = min(gps_time(1), ppm_time(1));
end_time = max(gps_time(end), ppm_time(end));
t = start_time:sample_period:end_time;

% Interpolate on to the common time base
gps_nvel = interp1(gps_time,gps_nvel,t,'linear',0);
gps_evel = interp1(gps_time,gps_evel,t,'linear',0);
gps_dvel = interp1(gps_time,gps_dvel,t,'linear',0);

thrust_cmd = interp1(ppm_time,thrust_cmd,t,'linear',0);



end