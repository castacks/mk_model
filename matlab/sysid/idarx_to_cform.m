function idarx_to_cform(model)

B = model.B;

disp('double coeff[] = {');
for n = 1:length(B)
    if(n == length(B))
        disp([num2str(B(1,1,n))]);
    else
        disp([num2str(B(1,1,n)) ',']);
    end
end
disp('};');

delay = model.InputDelay;

disp(['double delay = ' num2str(abs(delay)) ';']);

disp(['int count = ' num2str(length(B)) ';']);

end