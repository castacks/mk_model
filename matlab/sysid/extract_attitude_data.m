function [roll, pitch, yaw, roll_rate, pitch_rate, yaw_rate, roll_cmd, pitch_cmd, yaw_cmd, t] = extract_attitude_data(dir,sample_period)

euler = csvread([dir '/euler.csv'],1,0);
control = csvread([dir '/control.csv'],1.0);
nav = csvread([dir '/nav.csv'],1.0);

euler_time = euler(:,1);
roll = euler(:,4);
pitch = euler(:,5);
yaw = euler(:,6);

control_time = control(:,1);
roll_cmd = control(:,8);
pitch_cmd = control(:,7);
yaw_cmd = control(:,9);

nav_time = nav(:,1);
roll_rate = nav(:,66);
pitch_rate = nav(:,67);
yaw_rate = nav(:,68);

% yaw = unwrap(yaw);
% yaw = [0; diff(yaw)];

% Make the times manageable
euler_time = euler_time - control_time(1);
control_time = control_time - control_time(1);
nav_time = nav_time - nav_time(1) + euler_time(1);

% Convert nanoseconds to seconds
euler_time = euler_time/1e9;
control_time = control_time/1e9;
nav_time = nav_time/1e9;

% Create a time base
start_time = control_time(1);
end_time = control_time(end);
t = start_time:sample_period:end_time;

% Interpolate on to the common time base
roll_cmd = interp1(control_time,roll_cmd,t,'linear',0);
pitch_cmd = interp1(control_time,pitch_cmd,t,'linear',0);
yaw_cmd = interp1(control_time,yaw_cmd,t,'linear',0);

roll = interp1(euler_time,roll,t,'linear',0);
pitch = interp1(euler_time,pitch,t,'linear',0);
yaw = interp1(euler_time,yaw,t,'linear',0);

roll_rate = interp1(nav_time,roll_rate,t,'linear',0);
pitch_rate = interp1(nav_time,pitch_rate,t,'linear',0);
yaw_rate = interp1(nav_time,yaw_rate,t,'linear',0);

end