cd 
sample_period = 0.01;

%% Roll Modeling
[roll, ~, ~, roll_rate, ~, ~, roll_cmd, ~, ~, t_roll] = extract_attitude_data('roll',sample_period);

% roll = roll_rate;

rolldata = iddata(roll',roll_cmd',sample_period,'InputName','RollCmd','OutputName','Roll');

rolld = detrend(rolldata,0);
% roll_mdl = impulse(rolld,[-0.1 0.8],'PW',[]);
roll_mdl = n4sid(rolld,2);

pred_roll = sim(roll_mdl,rolldata);

roll_err = pred_roll.y - roll';
roll_err = roll_err.^2;
roll_err = sqrt(mean(roll_err))*180/pi;

figure;
hold on;
plot(t_roll,roll_cmd*-1.0,'r');
plot(t_roll,roll*180/pi,'g');
plot(t_roll,pred_roll.y*180/pi,'b');
title(['Roll (RMS Prediction Error: ' num2str(roll_err) ' degrees)']);
xlabel('Time (s)');
ylabel('Roll Angle (deg)');
legend('Command','Actual','Predicted');

%% Pitch Modeling
[~, pitch, ~, ~, pitch_rate, ~, ~, pitch_cmd, ~, t_pitch] = extract_attitude_data('pitch',sample_period);

pitchdata = iddata(pitch',pitch_cmd',sample_period,'InputName','PitchCmd','OutputName','Pitch');

pitchd = detrend(pitchdata,0);
% pitch_mdl = impulse(pitchd,[-0.1 0.8],'PW',[]);
pitch_mdl = n4sid(pitchd,2);

pred_pitch = sim(pitch_mdl,pitchdata);

pitch_err = pred_pitch.y - pitch';
pitch_err = pitch_err.^2;
pitch_err = sqrt(mean(pitch_err))*180/pi;

figure;
hold on;
plot(t_pitch,pitch_cmd*-1,'r');
plot(t_pitch,pitch*180/pi,'g');
plot(t_pitch,pred_pitch.y*180/pi,'b');
title(['Pitch (RMS Prediction Error: ' num2str(pitch_err) ' degrees)']);
xlabel('Time (s)');
ylabel('Pitch Angle (deg)');
legend('Command','Actual','Predicted');


%% Yaw Modeling
[~, ~, yaw, ~, ~, yaw_rate, ~, ~, yaw_cmd, t_yaw] = extract_attitude_data('yaw',sample_period);

yawdata = iddata(yaw_rate',yaw_cmd',sample_period,'InputName','YawRateCmd','OutputName','YawRate');

yawd = detrend(yawdata,0);
% yaw_mdl = impulse(yawd,[-0.1 2.0],'PW',[]);
yaw_mdl = n4sid(yawdata,1);

pred_yaw = sim(yaw_mdl,yawdata);

yaw_err = pred_yaw.y - yaw_rate';
yaw_err = yaw_err.^2;
yaw_err = sqrt(mean(yaw_err))*180/pi;

figure;
hold on;
plot(t_yaw,yaw_cmd,'r');
plot(t_yaw,yaw_rate*180/pi,'g');
plot(t_yaw,pred_yaw.y*180/pi,'b');
title(['Yaw (RMS Prediction Error: ' num2str(yaw_err) ' deg/sec)']);
xlabel('Time (s)');
ylabel('Yaw Rate (deg/sec)');
legend('Command','Actual','Predicted');
% 
% %% Velcity Modeling
% [roll, pitch, yaw, ~, ~, ~, ~, ~, ~, t_att] = extract_attitude_data('vel',sample_period);
% [thrust, gps_nvel, gps_evel, gps_dvel, t_vel] = extract_velocity_data('vel',sample_period);
% 
% % Denoise the yaw a bit...
% yaw = conv(yaw,0.1*ones(1,10));
% 
% vert_vel = -gps_dvel;
% horiz_vel = sqrt(gps_nvel.^2 + gps_evel.^2);
% 
% phi = zeros(size(gps_dvel));
% 
% for n=1:length(phi)
% 
%     c1 = cos(roll(n));
%     s1 = sin(roll(n));
%     c2 = cos(pitch(n));
%     s2 = sin(pitch(n));
%     c3 = cos(yaw(n));
%     s3 = sin(yaw(n));
%     
%     R = [c1*c2, c1*s2*s3-s1*c3, s1*s3+c1*s2*c3;
%          s1*c2, s1*s2*s3+c1*c3, s1*s2*c3-c1*s3;
%          -s2,   c2*s3,          c2*c3];
%     
%     vert = [0;0;1];
%     rot_vert = R*vert;
%     
%     phi(n) = acos(vert'*rot_vert);
%     
% end
% 
% horiz_vel = horiz_vel';
% vert_vel = vert_vel';
% phi = phi';
% thrust = thrust';
% 
% horiz_data = iddata(horiz_vel,[phi, thrust],sample_period);
% vert_data = iddata(vert_vel,[phi, thrust],sample_period);
% 
% %% Hover Modeling
% 
% [roll, pitch, yaw, ~, ~, ~, t_att] = extract_attitude_data('hover',sample_period);
% [thrust, gps_nvel, gps_evel, gps_dvel, t_vel] = extract_velocity_data('hover',sample_period);

save('models.mat','roll_mdl','pitch_mdl','yaw_mdl');



