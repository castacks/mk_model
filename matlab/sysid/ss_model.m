



load models.mat

g = 9.81;
Ts = 0.04;

A = zeros(6);
A(1,2) = 1;
A(3,4) = 1;
A(5,6) = 1;

B = zeros(6,4);
B(2,1) = -g;
B(2,2) = g;
B(4,1) = g;
B(6,2) = -g;

C = eye(6);

D = zeros(6,4);

vel_mdl = ss(A,B,C,D);
vel_mdl = c2d(vel_mdl,Ts);