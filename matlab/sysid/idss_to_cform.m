function idss_to_cform(model,name)

if(nargin == 1)
    name = '';
end

print_mat(model.A,[name 'A']);
print_mat(model.B,[name 'B']);
print_mat(model.C,[name 'C']);

end

function print_mat(M,name)

[nr,nc] = size(M);

disp(['Eigen::MatrixXd ' name '(' num2str(nr) ',' num2str(nc) ');']);
disp([name ' << ']);
for r=1:nr
    fprintf('  ');
    for c=1:nc
        fprintf('%d, ',M(r,c));
    end
    fprintf('\n');
end
fprintf('\b\b\b;\n\n');

end