#!/bin/bash
# Export topics to ascii for use in matlab
#Attitude modelling
rostopic echo -p --nostr /trajectory_control/odometry >odometry.csv &
rostopic echo -p --nostr /mikrokopter/req_set_control >command.csv &
rostopic echo -p --nostr /mikrokopter/ppm >spektrum.csv &
#Velocity controller 
rostopic echo -p --nostr /trajectory_control/command >velCommand.csv &
rostopic echo -p --nostr /trajectory_control/path >pathCommand.csv &


#Play the file:
rosbag play -q -r 3.0 --clock *.bag 

