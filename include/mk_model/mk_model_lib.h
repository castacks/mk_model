#ifndef _MK_MODEL_LIB_H_
#define _MK_MODEL_LIB_H_

#include <algorithm>
#include <deque>
#include <cstdlib>
#include <ca_common/math.h>
#include <mk_model/dynamicsPrimitives.h>
#include <mk_model/mk_common.h>

#include <ros/node_handle.h>

#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>


namespace CA
{
  class MkModelLibParameters
  {
  public:
    std::deque<double> thrust_coeff;
    double thrust_shift;
    double thrust_delay;
 
    double mass;
    double airDensity;
    double fx_disturbance;
    double fx_dist_stdev;
    double fy_disturbance;
    double fy_dist_stdev;
    double fz_disturbance;
    double fz_dist_stdev;
    double vel_fb_stdev;
    double euler_stdev; 
    double drag_constant;
    int trans_delay;
    double dt;
    Eigen::MatrixXd rollA;
    Eigen::MatrixXd rollB;
    Eigen::MatrixXd rollC;
    Eigen::MatrixXd pitchA;
    Eigen::MatrixXd pitchB;
    Eigen::MatrixXd pitchC;
    Eigen::MatrixXd yawA;
    Eigen::MatrixXd yawB;
    Eigen::MatrixXd yawC;

    MkModelLibParameters():
      rollA(2,2),
      rollB(2,1),
      rollC(1,2),
      pitchA(2,2),
      pitchB(2,1),
      pitchC(1,2),
      yawA(1,1),
      yawB(1,1),
      yawC(1,1)
    
    {
      setDefaults();
    }
    
    MkModelLibParameters(int model):
      rollA(2,2),
      rollB(2,1),
      rollC(1,2),
      pitchA(2,2),
      pitchB(2,1),
      pitchC(1,2),
      yawA(1,1),
      yawB(1,1),
      yawC(1,1)
    
    {
      setDefaults();
      if(model==2)//shipboard
      {
		thrust_coeff.clear();
      thrust_coeff.push_back(0.24306381);
      thrust_shift = -9.2232685714;
      thrust_delay = 10;
      airDensity   = 0.7;
      mass         = 2.10;
      drag_constant = 0.15;
      trans_delay = 2;


	 
//sysid from 2013_10_21_DATA positionHoldVicon8Auto
     rollA <<
     0.998,0.04897,
    -0.03867,0.3288;
      rollB <<
      0.00004958,
    -0.002286;
      rollC <<
	1.237,0.02327;
	
	
	  pitchA <<
     0.9975,0.07296,
    -0.04628,0.584;
      pitchB <<
      0.0001246,
    -0.001714;
      pitchC <<
	0.7416,0.02342;
	

	
      yawA <<
	9.571982e-01;
      yawB <<
	2.470240e-04;
      yawC <<
	2.379545e+00; 
	

	
	  }
    }
    
    bool loadParameters(ros::NodeHandle &n)
    {
      bool found = true;
      found = found && n.getParam("mass",              mass);
      found = found && n.getParam("airDensity",        airDensity);
      found = found && n.getParam("fx_disturbance",    fx_disturbance);
      found = found && n.getParam("fy_disturbance",    fy_disturbance);
      found = found && n.getParam("fz_disturbance",    fz_disturbance);
      found = found && n.getParam("fx_dist_stdev",     fx_dist_stdev);
      found = found && n.getParam("fy_dist_stdev",     fy_dist_stdev);
      found = found && n.getParam("fz_dist_stdev",     fz_dist_stdev);
      found = found && n.getParam("vel_fb_stdev",      vel_fb_stdev);
      found = found && n.getParam("euler_stdev",       euler_stdev);
      found = found && n.getParam("drag_constant",     drag_constant);
      found = found && n.getParam("translation_delay", trans_delay);

      //Optional inputs
      bool thrust_param = false;
      //n.param<bool>("thrust_param", thrust_param, false);
      if(n.getParam("thrust_param",  thrust_param))
      {
    	  if(thrust_param)
    	  {
			  double tcoeff=0;
			  n.getParam("thrust_coeff", tcoeff);
			  thrust_coeff.clear();
			  thrust_coeff.push_back(tcoeff);
			  n.getParam("thrust_shift", thrust_shift);
			  n.getParam("thrust_delay", thrust_delay);
    	  }
      }
      return found;
    }
  private:
    void setDefaults()
    {
      thrust_coeff.clear();
      thrust_coeff.push_back(0.4177);
      thrust_shift = -17.25;
      thrust_delay = 10;
      airDensity   = 1.0;
      mass         = 2.802;
      fx_disturbance =  0.0;
      fx_dist_stdev = 0.0;
      fy_disturbance = 0.0;
      fy_dist_stdev  = 0.0;
      fz_disturbance = 0.0;
      fz_dist_stdev =  0.0;
      vel_fb_stdev = 0.0;
      euler_stdev = 0.0;
      drag_constant = 0.15;
      trans_delay = 2;
      dt          = 0.01;
      rollA <<
	9.868662e-01, 1.562683e-02,
	-5.776124e-02, 8.712598e-01;
      rollB <<
	1.122620e-04,
	-1.903916e-03;
      rollC <<
	3.016132e+00, 1.966589e-02;
	
	 pitchA <<
	9.868662e-01, 1.562683e-02,
	-5.776124e-02, 8.712598e-01;
     pitchB <<
	1.122620e-04,
	-1.903916e-03;
     pitchC <<
	3.016132e+00, 1.966589e-02;
      yawA <<
	9.571982e-01;
      yawB <<
	2.470240e-04;
      yawC <<
	2.379545e+00;
    }
};


class MkModelLibState
{ 
public:
  MkModelLibParameters parameters;
  StateSpaceSim roll_sim;
  StateSpaceSim pitch_sim;
  StateSpaceSim yaw_sim;
  ArxSim thrust_sim;
  PureDelayState state_delayed;
  double time;
  MkModelLibState(State initialState,MkModelLibParameters p=MkModelLibParameters()):
    parameters(p),
    roll_sim(p.rollA,p.rollB,p.rollC),
    pitch_sim(p.pitchA,p.pitchB,p.pitchC*-1.0),
    yaw_sim(p.yawA,p.yawB,p.yawC),
    thrust_sim(p.thrust_coeff,p.thrust_delay,1.0,p.thrust_shift),
    state_delayed(p.trans_delay,initialState),
    time(0.0)
  {
  }
};


class MkModelLib
{
 protected:
  boost::mt19937 rand_seed;
  boost::variate_generator<boost::mt19937, boost::normal_distribution<> > rand_norm;
 public:
 MkModelLib():
  rand_norm(rand_seed, boost::normal_distribution<>(0.0, 1.0))
      {}
  CA::State step( MkModelLibState &state,MkModelLibCommand &command);
};

}
#endif






