#ifndef _MK_COMMON_H_
#define _MK_COMMON_H_

#include <ca_common/math.h>
#include <cmath>

namespace CA
{
  class MkModelLibCommand
  {
  public:
    double roll;   // In Mikrokopter units
    double pitch;  // In Mikrokopter units
    double yaw;    // In Mikrokopter units
    double thrust; // In Mikrokopter units
    MkModelLibCommand():
      roll(0),pitch(0),yaw(0),thrust(154.0)
    {}
  };

  class MkVelocityControlCommand
  {
  public:
    double heading;
    double headingrate;
    CA::Vector3D velocity;
    CA::Vector3D acceleration;
    MkVelocityControlCommand():
      heading(0.0),headingrate(0.0),
      velocity(0,0,0),acceleration(0,0,0)
    {}
      bool isfinite()
      {
	return std::isfinite(heading) && CA::isfinite(velocity) && CA::isfinite(acceleration) && std::isfinite(headingrate);
      }
  };
  class MkVelocityControlState
  {
  public:
    double vxIntegrator;
    double vyIntegrator;
    double vzIntegrator;
    double prev_xd_err;
    double prev_yd_err;
    double prev_zd_err;
    MkVelocityControlState()
    {
      reset();
    }
    void reset()
    {
      vxIntegrator = 0.0;
      vyIntegrator = 0.0;
      vzIntegrator = 0.0;
      prev_xd_err  = 0.0;
      prev_yd_err  = 0.0;
      prev_zd_err  = 0.0;
    }
    bool isfinite()
    {
      return std::isfinite(vxIntegrator) && std::isfinite(vyIntegrator) && std::isfinite(vzIntegrator) && std::isfinite(prev_xd_err) && std::isfinite(prev_yd_err) && std::isfinite(prev_zd_err);
    }
  };

  class TrajectoryControlState
  {
  public:
    double alongTrackIntegrator;
    double crossTrackIntegrator;
    double prev_along_track_error;
    double prev_cross_track_error;
    double prev_z_track_error;
    double closestIdx;
    TrajectoryControlState():
      alongTrackIntegrator(0.0),
      crossTrackIntegrator(0.0),
      prev_along_track_error(0.0),
      prev_cross_track_error(0.0),
	prev_z_track_error(0.0),
      closestIdx(0.0)
    {}
  };
}


#endif
