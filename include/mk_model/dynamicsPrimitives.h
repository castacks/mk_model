#ifndef DYNAMICS_PRIMITIVES_H_
#define DYNAMICS_PRIMITIVES_H_


#include <ca_common/math.h>

#include <deque>

namespace CA
{

  class StateSpaceSim
  {
  public:
    StateSpaceSim(Eigen::MatrixXd A, Eigen::MatrixXd B, Eigen::MatrixXd C):
      A(A),
      B(B),
      C(C)
    {
      X = Eigen::MatrixXd::Zero(A.rows(),1);
    }
    
    inline double push(double input)
    {
      Eigen::MatrixXd out;
      out = C*X;
      
    X = A*X + B*input;
    
    return out(0,0);
    }

    inline double current()
    {
      Eigen::MatrixXd out;
      out = C*X;
      return out(0,0);
    }
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    private:
    Eigen::MatrixXd X;
    Eigen::MatrixXd A;
    Eigen::MatrixXd B;
    Eigen::MatrixXd C;
  };
  
  class ArxSim
  {
  public:
    ArxSim(std::deque<double> coefficients, unsigned int input_delay = 0, double gain = 1.0, double shft = 0.0):
      shift(shft)
    {
      coeffs = coefficients;
      for(unsigned int i=0; i < coefficients.size(); i++)
		{
		  coeffs[i] *= gain;
		}
      for(unsigned int i=0; i < coefficients.size()+input_delay; i++)
		{
		  inputs.push_back(0.0);
		}
    }
    
    inline double push(double input)
    {
      inputs.push_back(input);
      inputs.pop_front();
      
      double out = 0.0;
      
      for(unsigned int i=0; i<coeffs.size(); i++)
	{
	  out += coeffs[i]*inputs[i];
	}
      return out+shift;
    }

  private:
    double shift;
    std::deque<double> coeffs;
    std::deque<double> inputs;
};
  
  template<class T>
  class PureDelay
  {
  public:
    PureDelay(int input_delay,T initialValue)
    {
      input_delay = std::max(input_delay,1);
      for(int i=0;i<input_delay;++i)
	{
	  buffer.push_back(initialValue);
	}
    }
    inline T push(T val)
    {
      buffer.push_back(val);
      buffer.pop_front();
      return buffer.front();
    }
    inline T front()
    {
      return buffer.front();
    }
    inline T back()
    {
      return buffer.back();
    }
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    private:
    std::deque< T > buffer;
  };
  
  typedef PureDelay<CA::Vector3D>     PureDelay3D;
  typedef PureDelay<CA::QuatD>  PureDelayQuatD;
  typedef PureDelay<double>           PureDelayDouble;
  typedef PureDelay<int>              PureDelayInt;
  typedef PureDelay<CA::State>        PureDelayState;
  
}

#endif
