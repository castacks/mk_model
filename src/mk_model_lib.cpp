

#include <mk_model/mk_model_lib.h>
#include <cmath>


using namespace CA;

CA::State CA::MkModelLib::step( MkModelLibState &state,MkModelLibCommand &command)
{
  //Discretize command to represent the mikrokopter behavior:
  command.thrust = round(command.thrust);
  command.roll   = round(command.roll);
  command.pitch  = round(command.pitch);
  command.yaw    = round(command.yaw);
  CA::State pose,delayedPose;
  CA::Vector3D force,position,orientation, orientation_prev;
  double thrust,roll,pitch,yaw;
  pose = state.state_delayed.back();
  //
  // Attitude Dynamics
  //
  orientation_prev = pose.pose.orientation_rad;
  pose.pose.orientation_rad[0]  = state.roll_sim.push(command.roll);
  pose.pose.orientation_rad[1]  = state.pitch_sim.push(command.pitch);
  pose.pose.orientation_rad[2] += state.yaw_sim.push(command.yaw) * state.parameters.dt;


  //
  // Linear Force - Thrust Component
  //
  // - Calculate the expected translational forces on the vehicle based on this new attitude
  roll   = pose.pose.orientation_rad[0];
  pitch  = pose.pose.orientation_rad[1];
  yaw    = pose.pose.orientation_rad[2];
  double nThrust = (state.parameters.airDensity -0.7) * ( -10.0 ) + command.thrust;
  thrust =  state.thrust_sim.push(nThrust);
  if(thrust<0.0)
    thrust = 0.0;
  //  ROS_INFO_STREAM(command.thrust<<" nthrust "<<nThrust<<" Force: "<<thrust);
  force[0] = -thrust * ( sin(yaw)*sin(roll) - cos(yaw)*sin(pitch)*cos(roll));
  force[1] = -thrust * (-cos(yaw)*sin(roll) - sin(yaw)*sin(pitch)*cos(roll));
  force[2] = -thrust * cos(roll) * cos(pitch);
  
  //
  // Linear Force - Gravity Component
  //
  // - Add the force of gravity in the z direction
  force[2] += state.parameters.mass * CA::math_consts::GRAVITY;
  
  //
  // Linear Force - Drag Component
  //
  // - Subtract the draft from air based on current velocity
  force -= state.parameters.drag_constant * pose.rates.velocity_mps;
  
  //
  // Linear Force Disturbance
  //
  // - Add in a constant force offset (might be used to model wind)
  force[0] += state.parameters.fx_disturbance + state.parameters.fx_dist_stdev*rand_norm();
  force[1] += state.parameters.fy_disturbance + state.parameters.fy_dist_stdev*rand_norm();
  force[2] += state.parameters.fz_disturbance + state.parameters.fz_dist_stdev*rand_norm();
  
  //
  // Linear Velocity Integration
  //
  // - Time update of the velocity
  pose.rates.velocity_mps += (force / state.parameters.mass) * state.parameters.dt;
  
  //
  // Linear Position Integration
  //
  // - Time update of the position
  // double origz = pose.pose.position_m[2];
  pose.pose.position_m += pose.rates.velocity_mps * state.parameters.dt;

  delayedPose = state.state_delayed.push(pose);

  //  ROS_INFO_STREAM(origz<<" "<<pose.pose.position_m[2]<<" "<<delayedPose.pose.position_m[2]<<";");

  //Potentially corrupt the result with noise
  delayedPose.rates.velocity_mps   += state.parameters.vel_fb_stdev * Eigen::Vector3d(rand_norm(), rand_norm(), rand_norm());
  delayedPose.pose.orientation_rad +=  state.parameters.euler_stdev * Eigen::Vector3d(rand_norm(), rand_norm(), rand_norm());
  delayedPose.rates.rot_rad = (1/state.parameters.dt)*(delayedPose.pose.orientation_rad-orientation_prev);
  state.time += state.parameters.dt;
  delayedPose.time_s = state.time;
  
  return delayedPose;
}


