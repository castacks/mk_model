#include "ros/ros.h"
#include "mikrokopter/Control.h"
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Float64.h>
#include <nav_msgs/Odometry.h>
#include <rosgraph_msgs/Clock.h>
#include <ca_common/math.h>
#include <cstdlib>
#include <algorithm>
#include <deque>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <mk_model/mk_model_lib.h>


using namespace CA;

bool running = false;

MkModelLibCommand command;

void controlCallback(const mikrokopter::Control::ConstPtr& msg)
{
  command.roll   = (double) (*msg).roll;
  command.pitch  = (double) (*msg).pitch;
  command.yaw    = (double) (*msg).yaw;
  command.thrust = (double) (*msg).thrust;
  running = true;
}
int main(int argc, char **argv)
{
  ros::init(argc, argv, "mk_model");
  ros::NodeHandle n("mk_model");
  CA::State initialPose;
  bool publish_clock = true;
  
	n.param("initial_x", initialPose.pose.position_m[0], 0.0);
	n.param("initial_y", initialPose.pose.position_m[1], 0.0);
	n.param("initial_z", initialPose.pose.position_m[2], 0.0);
	n.param("initial_heading", initialPose.pose.orientation_rad[2], 0.0);  
  
	n.param("publish_clock", publish_clock, true);
  
  rosgraph_msgs::Clock clockMsg;
  ros::Publisher pubClock;
  if(publish_clock){
  	ROS_WARN_STREAM("mk_model, will control the clock.");
  	pubClock = n.advertise<rosgraph_msgs::Clock>("/clock",10);
	}
  else
  	ROS_WARN_STREAM("mk_model, will not control the clock.");
  
  command.thrust = 154;  

  
  ros::Subscriber sub = n.subscribe<mikrokopter::Control>("command", 1, controlCallback);
  ros::Publisher state_pub    = n.advertise<nav_msgs::Odometry>       ("odometry", 100);
  //ros::Publisher vel_pub      = n.advertise<geometry_msgs::Vector3>   ("velocity_fb", 100);
  //  ros::Publisher real_vel_pub = n.advertise<geometry_msgs::Vector3>   ("real_velocity_fb",100);
  //ros::Publisher ori_pub      = n.advertise<geometry_msgs::Quaternion>("orientation_fb", 100);
  //
  int model;
  n.param("model", model, 1);
  MkModelLibParameters params(model);
  if(!params.loadParameters(n))
    {
      ROS_ERROR_STREAM("Was not able to get all initial parameters");
      return -1;
    }

  MkModelLibState state(initialPose,params);
  MkModelLib simulator;
  ros::WallRate loop_rate(1/(state.parameters.dt));
  double offset = ros::WallTime::now().toSec();
  CA::State current;
  while (ros::ok())
  {  
    if(running)
    {
     
      current = simulator.step(state,command);

			// Convert world velocity to body velocity for ROS Odometry msg conventions
			current.rates.velocity_mps = (eulerToQuat(current.pose.orientation_rad)).inverse()*current.rates.velocity_mps;

      nav_msgs::Odometry odom_out = msgc(current);
      odom_out.header.frame_id    = "/world";
      odom_out.child_frame_id     = "/mk";
      odom_out.header.stamp       = ros::Time(current.time_s+offset);
      state_pub.publish(odom_out);
      //vel_pub.publish(odom_out.twist.twist.linear);
      //ori_pub.publish(odom_out.pose.pose.orientation);
    
    }
    else
    {
			offset+=state.parameters.dt;
    }
      
    if(publish_clock)
    {
    	clockMsg.clock = ros::Time(current.time_s+offset);
    	pubClock.publish(clockMsg);
    }
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
